package com.endare.bles;

import android.content.Intent;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.os.AsyncTask;
import android.util.Log;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;

/**
 * Created by Francisco on 31/03/16.<br>
 * this class handles the parsing of nfctags. <br>
 *     NFC tags can work on two ways: <br>
 *     -1- the system detects a nfctag and triggers the activity, this should be declared in your manifest.
 *     You then receive the data by catching the intent provided by the system and give it to the <strong>readNFC</strong> method. <br>
 *     -2- if your app should only listen for nfctags at a certain moment in the lifecycle your activity should implement
 *     the <strong>NfcAdapter.ReaderCallback</strong> and when the <strong>onTagDiscovered</strong> method is called you can parse
 *     the data by handing over the Tag to the <strong>getDataFromTag</strong> method.<br><br>
 *         for more info visit <a href="http://developer.android.com/guide/topics/connectivity/nfc/index.html">developer.android.com</a>
 */
public class NFCReader {
    private NFCDataListener dataListener;
    private static final String MIME_TEXT_PLAIN = "text/plain";
    private ReturnData callback;

    /**
     * retrieves the data from a nfc chip when the app is triggerd by an intent
     * @param intent
     * the intent provided by the system containing the nfc tag
     * @param nfcDataListener
     * an instance of the nfcDataListener which handles the return of the data.<br>
     *     if the dataset equals a "int && int" style, the method will return two integers, else the method
     *     will return a string containing the data on the chip.
     */
    public void readNFC(Intent intent, NFCDataListener nfcDataListener){
        this.dataListener = nfcDataListener;
        String action = intent.getAction();
        if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action)) {
            String type = intent.getType();
            if (MIME_TEXT_PLAIN.equals(type)) {
                Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
                new NFCBackgroundTask().execute(tag);
                callback = new ReturnData() {
                    @Override
                    public void returnData(String data) {
                        String[] parts = data.split("&&");
                        if (parts.length == 2){
                            int id2 = Integer.parseInt(parts[0]);
                            int id3 = Integer.parseInt(parts[1]);
                            dataListener.ParsedIDsFromNFCListener(id2, id3);
                        } else {
                            dataListener.NFCDataListener(data);
                        }
                    }
                };

            } else {
                Log.e("NFC", "Wrong mime type: " + type);
            }
        }
    }

    /**
     * retrieves the data stored on the NFC chip
     * @param tag
     * The tagobject containing the data
     * @param nfcDataListener
     * an instance of the nfcDataListener which handles the return of the data.<br>
     *     if the dataset equals a "int && int" style, the method will return two integers, else the method
     *     will return a string containing the data on the chip.
     */
    public void getDataFromTag(Tag tag, NFCDataListener nfcDataListener){
        this.dataListener = nfcDataListener;
        new NFCBackgroundTask().execute(tag); //start an asynctTask that parses the tag
        callback = new ReturnData() {
            @Override
            public void returnData(String data) {
                if (data.equals("ERROR")){
                    dataListener.NFCnotSupported();
                } else {
                    String[] parts = data.split("&&");
                    if (parts.length == 2){
                        int id2 = Integer.parseInt(parts[0]);
                        int id3 = Integer.parseInt(parts[1]);
                        dataListener.ParsedIDsFromNFCListener(id2, id3);
                    } else {
                        dataListener.NFCDataListener(data);
                    }
                }
            }
        };
    }

    /**
     * the asynctask that handles the parsing of the tag
     */
    private class NFCBackgroundTask extends AsyncTask<Tag, Void, String> {

        @Override
        protected String doInBackground(Tag... params) {
            Tag tag = params[0];

            Ndef ndef = Ndef.get(tag);
            if (ndef == null) {
                // NDEF is not supported by this Tag.
                Log.e("error NFC","NDEF is not supported by this Tag");
                return "ERROR";
            }

            NdefMessage ndefMessage = ndef.getCachedNdefMessage();

            NdefRecord[] records = ndefMessage.getRecords();
            for (NdefRecord ndefRecord : records) {
                if (ndefRecord.getTnf() == NdefRecord.TNF_WELL_KNOWN && Arrays.equals(ndefRecord.getType(), NdefRecord.RTD_TEXT)) {
                    try {
                        return readText(ndefRecord);
                    } catch (UnsupportedEncodingException e) {
                        Log.e("NFC", "Unsupported Encoding", e);
                    }
                }
            }

            return null;
        }
        private String readText(NdefRecord record) throws UnsupportedEncodingException {
        /*
         * See NFC forum specification for "Text Record Type Definition" at 3.2.1
         *
         * http://www.nfc-forum.org/specs/
         *
         * bit_7 defines encoding
         * bit_6 reserved for future use, must be 0
         * bit_5..0 length of IANA language code
         */

            byte[] payload = record.getPayload();

            // Get the Text Encoding
            String textEncoding = ((payload[0] & 128) == 0) ? "UTF-8" : "UTF-16";

            // Get the Language Code
            int languageCodeLength = payload[0] & 0063;

            // String languageCode = new String(payload, 1, languageCodeLength, "US-ASCII");
            // e.g. "en"

            // Get the Text
            return new String(payload, languageCodeLength + 1, payload.length - languageCodeLength - 1, textEncoding);
        }

        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
                callback.returnData(result);
            }
        }
    }

    private interface ReturnData{
        void returnData(String data);
    }

}