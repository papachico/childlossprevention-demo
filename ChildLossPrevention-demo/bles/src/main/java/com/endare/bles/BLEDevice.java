package com.endare.bles;

import java.io.Serializable;

/**
 * Created by Francisco on 31/03/16.
 * This class represents an object used to define BLEDevices.
 */
public class BLEDevice implements Serializable {
    private String name;
    private int id2, id3;
    private boolean inRange;

    public BLEDevice(String name, int id2, int id3, boolean inRange) {
        this.name = name;
        this.id2 = id2;
        this.id3 = id3;
        this.inRange = inRange;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId2() {
        return id2;
    }

    public void setId2(int id2) {
        this.id2 = id2;
    }

    public int getId3() {
        return id3;
    }

    public void setId3(int id3) {
        this.id3 = id3;
    }

    public boolean isInRange() {
        return inRange;
    }

    public void setInRange(boolean inRange) {
        this.inRange = inRange;
    }

    public String toString(){
        return this.getName() + " is in range: " + this.isInRange();
    }
}
