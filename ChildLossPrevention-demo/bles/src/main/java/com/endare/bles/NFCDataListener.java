package com.endare.bles;

/**
 * Created by Francisco on 31/03/16.
 */
public interface NFCDataListener {
    void NFCDataListener(String data);
    void ParsedIDsFromNFCListener(int id2, int id3);
    void NFCnotSupported();
}
