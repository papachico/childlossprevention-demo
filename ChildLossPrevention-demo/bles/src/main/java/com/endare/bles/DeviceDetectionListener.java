package com.endare.bles;

import com.estimote.sdk.Beacon;

public interface DeviceDetectionListener {
    /**
     * gets triggered when a BLE device matching the region specifics connects to the app
     * @param beacon
     * the beacon object containing the info of the BLE device
     */
    void deviceEnterListener(Beacon beacon);

    /**
     * gets triggered when a BLE device matching the region specifics and in which the app has declared
     * interest connects to the app
     * @param beacon
     * the beacon object containing the info of the BLE device
     * @param name
     * the name under which the BLE device is registered as an interessed device
     */
    void interestedDeviceEnterListener(Beacon beacon, String name);

    /**
     * gets triggered when a BLE device matching the region specifics disconnects from the app
     * @param beacon
     * the beacon object containing the info of the BLE device
     */
    void deviceLeftListener(Beacon beacon);

    /**
     * gets triggered when a BLE device matching the region specifics and in which the app has declared
     * interest disconnects from the app
     * @param beacon
     * the beacon object containing the info of the BLE device
     * @param name
     * the name under which the BLE device is registered as an interessed device
     */
    void interestedDeviceLeftListener(Beacon beacon, String name);
}
