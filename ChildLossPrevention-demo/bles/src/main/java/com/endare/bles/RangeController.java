package com.endare.bles;

import android.content.Context;
import android.util.Log;

import com.estimote.sdk.Beacon;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.StreamCorruptedException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Francisco on 31/03/16.<br>
 * this class contains basic logic to enable easy rangecontrol of <strong>BLEDevices</strong>
 */
public class RangeController {
    private Context context;
    private static String FILENAME = "interestedDevices";

    public RangeController(Context context) {
        this.context = context;
    }

    /**
     * checks if a device is already registered and saves it to the device's storage if not
     * @param name
     * the name of the device
     * @param id2
     * the major or id2 of the device
     * @param id3
     * the minor or id3 of the device
     * @return
     * returns false if the device was already registered, true if not.
     */
    public boolean setInterestInDevice(String name, int id2, int id3){
        ArrayList<BLEDevice> devices = retrieveInterestedDevices();
        BLEDevice newDevice = new BLEDevice(name, id2, id3, checkIfConnected(id2, id3));
        if (devices.contains(newDevice)){
            return false;
        } else {
            devices.add(newDevice);
            saveArrayListOfBLEDevices(devices);
            return true;
        }
    }

    /**
     * change the name of an interested device
     * @param id2
     * the major or id2 of the device you want to alter
     * @param id3
     * the minor or id3 of the device you want to alter
     * @param newName
     * the new name you want to give to the device
     * @return
     * returns true if succeeded, false if not
     */
    public boolean changeNameInterestedDevice(int id2, int id3, String newName){
        ArrayList<BLEDevice> devices = retrieveInterestedDevices();
        for (int i = 0; i < devices.size(); i++){
            if (id2 == devices.get(i).getId2() && id3 == devices.get(i).getId3()){
                devices.get(i).setName(newName);
                saveArrayListOfBLEDevices(devices);
                return true;
            }
        }
        return false;
    }

    /**
     * returns an ArrayList of <strong>BLEDevices</strong> that where previously registered.
     * @return
     * returns an ArrayList of <strong>BLEDevices</strong>, if no devices were registered it will return an empty ArrayList.
     */
    public ArrayList<BLEDevice> retrieveInterestedDevices(){
        ArrayList<BLEDevice> devices;
        try {
            FileInputStream fis = context.openFileInput(FILENAME);
            ObjectInputStream ois = new ObjectInputStream(fis);
            devices = (ArrayList<BLEDevice>) ois.readObject();
            fis.close();
            ois.close();
            return devices;
        } catch (FileNotFoundException e) {
            return new ArrayList<>();
        } catch (StreamCorruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    /**
     * checks if a device was previously registered by matching the major and minor
     * @param id2
     * the major or id2 of the device
     * @param id3
     * the minor or id3 of the device
     * @return
     * returns a BLEDevice if the devices was registered (so you can acces the other properties if needed), null if not.
     */
    public BLEDevice checkIfInterested(int id2, int id3){
        ArrayList<BLEDevice> devices = retrieveInterestedDevices();
        for (int i = 0; i < devices.size(); i++){
            if (devices.get(i).getId2() == id2 && devices.get(i).getId3() == id3){
                return devices.get(i);
            }
        }
        return null;
    }

    /**
     * removes the interest in a device by providing an index
     * @param index
     * the index of the device in the ArrayList you want to remove
     */
    public void removeInterestInDeviceForIndex(int index){
        ArrayList<BLEDevice> devices = retrieveInterestedDevices();
        devices.remove(index);
        saveArrayListOfBLEDevices(devices);
    }

    /**
     * remove al interested devices
     */
    public void removeAllInterests(){
        saveArrayListOfBLEDevices(new ArrayList<BLEDevice>());
    }

    /**
     * validate a pairing code <br> see <strong>calculatePairingCode</strong> for more info about
     * calculation of the code
     * @param code
     * the code you want to validate
     * @param id2
     * the major or id2 of the device linked with the code
     * @param id3
     * the minor or the id3 of the device linked with the code
     * @return
     * returns true if the code is a valid one, false if not
     */
    public boolean validatePairingCode(String code, int id2, int id3){
        String resultString = calculatePairingCode(id2, id3);
        if (code.equals(resultString)){
            return true;
        } else {
            return false;
        }
    }

    /**
     * returns a pairing code for a device's id2 and id3 of a beacon by performing a mathematical calculation
     * on the id2 and id3
     * @param id2
     * the major or id2 of the device you want to get a pairing code for
     * @param id3
     * the minor or id3 of the device you want to get a pairing code for
     * @return
     * returns a pairingcode as a String
     */
    public String calculatePairingCode(int id2, int id3){
        int result = id2 * id3 / 95 + 1711;
        String resultString = String.valueOf(result);
        if (resultString.length() > 4){
            resultString = resultString.substring(0, 4);
        } else if (resultString.length() == 1){
            resultString += "0";
        } else if (resultString.length() == 2){
            resultString += "00";
        } else if (resultString.length() == 3){
            resultString += "00";
        }
        return resultString;
    }

    /**
     * check if the device is in range
     * @param major
     * the major or id2 of the device you want to check
     * @param minor
     * the minor or id3 of the device you want to check
     * @return
     * returns true if the device is in range, false if not
     */
    public boolean checkIfConnected(int major, int minor){
        List<Beacon> list = Scanner.beaconList;
        for (int i = 0; i < list.size(); i++){
            if (list.get(i).getMajor() == major && list.get(i).getMinor() == minor){
                return true;
            }
        }
        return false;
    }

    /**
     * changes the <strong>inRange</strong> flag of a specific interested device
     * @param id2
     * the major or id2 of the device you want to alter
     * @param id3
     * the minor or id3 of the device you want to alter
     * @param inRange
     * true if the device is in range, false if not
     */
    public boolean updateInRange(int id2, int id3, boolean inRange){
        ArrayList<BLEDevice> list = retrieveInterestedDevices();
        for (int i = 0; i < list.size(); i++){
            if (list.get(i).getId2() == id2 && list.get(i).getId3() == id3){
                list.get(i).setInRange(inRange);
                saveArrayListOfBLEDevices(list);
                return true;
            }
        }
        return false;
    }

    private void saveArrayListOfBLEDevices(ArrayList<BLEDevice> list){
        try {
            OutputStream os = context.openFileOutput(FILENAME, Context.MODE_PRIVATE);
            ObjectOutputStream oos = new ObjectOutputStream(os);
            oos.writeObject(list);
            os.close();
            oos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
