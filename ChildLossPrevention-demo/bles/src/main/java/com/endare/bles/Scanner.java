package com.endare.bles;


import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.Region;

import java.util.List;
import java.util.UUID;


/**
 * Created by Francisco on 29/03/16.
 * BLES stands for Bluetooth Low Energy Scanner
 * This library is made for discovering new BLE devices more easily.
 */
public class Scanner {
    private BeaconManager bm;
    private Region region = new Region("beacon", UUID.fromString("fe913213-b311-4a42-8c16-47faeac938db"), null, null); //change the region
    public static List<Beacon> beaconList;
    private RangeController rangeController;


    public Scanner(Context context) {
        bm = new BeaconManager(context);
        rangeController = new RangeController(context);
    }

    public Scanner(Context context, Region region) {
        bm = new BeaconManager(context);
        rangeController = new RangeController(context);
        this.region = region;
    }

    /**
     * start scanning for BLE devices matching the set region
     * @param ddl
     * the trigger that gets called when a device event occurs
     * @return
     * returns true if scanning is possible, false if not (e.g. when the device doesn't support bluetooth it will return false)
     */
    public boolean startScanning(final DeviceDetectionListener ddl){
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter == null){
            return false;
        } else {
            boolean state = bluetoothAdapter.enable();
            if (state){
                bm.connect(new BeaconManager.ServiceReadyCallback() {
                    @Override
                    public void onServiceReady() {
                        bm.startRanging(region);
                    }
                });

                bm.setRangingListener(new BeaconManager.RangingListener() {
                    @Override
                    public void onBeaconsDiscovered(Region region, List<Beacon> list) {
                        if (beaconList == null){
                            beaconList = list;
                        } else if (beaconList.size() < list.size()){
                            for (int x = 0; x <list.size(); x++){
                                if (!beaconList.contains(list.get(x))){
                                    BLEDevice device = rangeController.checkIfInterested(list.get(x).getMajor(), list.get(x).getMinor());
                                    if (device != null){
                                        ddl.interestedDeviceEnterListener(list.get(x), device.getName());
                                        rangeController.updateInRange(list.get(x).getMajor(), list.get(x).getMinor(), true);
                                    } else {
                                        ddl.deviceEnterListener(list.get(x));
                                    }
                                }
                            }
                            beaconList = list;
                        } else if (beaconList.size() > list.size()){ //a beacon has left
                            if (list.size() == 0){ //if no beacons are in range
                                BLEDevice device = rangeController.checkIfInterested(beaconList.get(0).getMajor(), beaconList.get(0).getMinor());
                                if (device != null){
                                    ddl.interestedDeviceLeftListener(beaconList.get(0), device.getName());
                                    rangeController.updateInRange(beaconList.get(0).getMajor(), beaconList.get(0).getMinor(), false);
                                } else {
                                    ddl.deviceLeftListener(beaconList.get(0));
                                }
                            } else {
                                for (int y = 0; y < beaconList.size(); y++){
                                    if (!list.contains(beaconList.get(y))){
                                        BLEDevice device = rangeController.checkIfInterested(beaconList.get(y).getMajor(), beaconList.get(y).getMinor());
                                        if (device != null){
                                            ddl.interestedDeviceLeftListener(beaconList.get(y), device.getName());
                                            rangeController.updateInRange(beaconList.get(y).getMajor(), beaconList.get(y).getMinor(), false);
                                        } else {
                                            ddl.deviceLeftListener(beaconList.get(y));
                                        }
                                    }
                                }
                            }
                            beaconList = list;
                        }
                    }
                });
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * stop the scanning!
     */
    public void stopScanning(){
        if (bm != null){
            bm.stopRanging(region);
        }
    }

    /**
     * change the region corresponding the BLE devices
     * @param region
     * a region which defines a UUID, Major & Minor
     */
    public void setRegion(Region region){
        this.region = region;
    }

}
