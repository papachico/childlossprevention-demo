package com.endare.childlossprevention_demo;

import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.estimote.sdk.Beacon;

import java.util.ArrayList;

/**
 * Created by Francisco on 06/04/16.
 */
public class NearbyFragment extends Fragment {
    private ArrayList<Beacon> connectedDevices;
    private Context context;
    private LayoutInflater inflater;
    private BroadcastReceiver updateReceiver;
    private LocalBroadcastManager broadcastManager;
    private IntentFilter intentFilter;
    private ProgressBar scanning;
    public static Beacon selectedBeacon;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.fragment_nearby, container, false);
        context = rootview.getContext();
        this.inflater = inflater;

        connectedDevices = BackgroundScanning.devicesInRange;
        if (connectedDevices == null){
            connectedDevices = new ArrayList<>();
        }
        final CustomAdapter adapter = new CustomAdapter(connectedDevices);
        ListView list = (ListView) rootview.findViewById(R.id.listView);
        list.setAdapter(adapter);

        intentFilter = new IntentFilter();
        intentFilter.addAction("com.endare.updateConnectedDevices");
        broadcastManager = LocalBroadcastManager.getInstance(context);
        updateReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                connectedDevices = BackgroundScanning.devicesInRange;
                if (connectedDevices.size() == 0){
                    scanning.setVisibility(View.VISIBLE);
                } else {
                    scanning.setVisibility(View.GONE);
                }
                adapter.notifyDataSetChanged();
            }
        };

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                AddNearbyDialog dialog = new AddNearbyDialog();
                selectedBeacon = connectedDevices.get(position);
                dialog.show(getFragmentManager(), "dialog_add_nearby");
            }
        });

        scanning = (ProgressBar) rootview.findViewById(R.id.scanning);


        Button btnNFC = (Button) rootview.findViewById(R.id.btnNFC);
        btnNFC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new FragmentAnimator().slideRightFragment(getFragmentManager().beginTransaction(), new NFCFragment(), R.id.newDeviceContainer);
            }
        });
        if (!NewDeviceActivity.nfcAvailable){
            View nfcView = rootview.findViewById(R.id.NFCView);
            nfcView.setVisibility(View.GONE);
        }
        return rootview;
    }

    @Override
    public void onResume() {
        super.onResume();
        broadcastManager.registerReceiver(updateReceiver, intentFilter);
    }

    @Override
    public void onPause() {
        super.onPause();
        broadcastManager.unregisterReceiver(updateReceiver);
    }

    public class CustomAdapter extends ArrayAdapter {
        ArrayList<Beacon> arrayList;
        public CustomAdapter(ArrayList<Beacon> arrayList) {
            super(context, R.layout.item_nearby, arrayList);
            this.arrayList = arrayList;
        }

        @Override
        public int getCount() {
            return arrayList.size();
        }

        @Override
        public Object getItem(int position) {
            return arrayList.get(position);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = inflater.inflate(R.layout.item_nearby, null);

            TextView txtMac = (TextView) convertView.findViewById(R.id.txtMac);
            TextView txtMajor = (TextView) convertView.findViewById(R.id.txtMajor);
            TextView txtMinor = (TextView) convertView.findViewById(R.id.txtMinor);

            txtMac.setText(arrayList.get(position).getMacAddress().toString());
            txtMajor.setText(arrayList.get(position).getMajor() + "");
            txtMinor.setText(arrayList.get(position).getMinor() + "");


            return convertView;
        }
    }
}
