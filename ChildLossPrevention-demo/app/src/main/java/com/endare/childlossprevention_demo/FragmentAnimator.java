package com.endare.childlossprevention_demo;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.view.View;

/**
 * Created by Francisco on 06/04/16.
 */
public class FragmentAnimator {

    public void slideRightFragment(FragmentTransaction ft, Fragment newFragment, int container){
        ft.setCustomAnimations(R.animator.slide_in, R.animator.slide_out);
        ft.replace(container, newFragment);
        ft.commit();
    }

    public void slideLeftFragment(FragmentTransaction ft, Fragment newFragment, int container){
        ft.setCustomAnimations(R.animator.reverse_slide_in, R.animator.reverse_slide_out);
        ft.replace(container, newFragment);
        ft.commit();
    }
}
