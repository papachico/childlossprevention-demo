package com.endare.childlossprevention_demo;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.nfc.Tag;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.endare.bles.NFCDataListener;
import com.endare.bles.NFCReader;
import com.endare.bles.RangeController;

/**
 * Created by Francisco on 06/04/16.
 */
public class NFCRegistrationFragment extends Fragment {
    private int major, minor;
    private Context context;
    private EditText editName;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.fragment_nfc_registration, container, false);
        context = rootview.getContext();
        editName = (EditText) rootview.findViewById(R.id.editName);

        manageButtonBar(rootview);
        handleTag(rootview);
        return rootview;
    }

    private void handleTag(View view){
        NFCReader reader = new NFCReader();
        Tag tag = NewDeviceActivity.discoveredTag;
        final TextView txtMajor = (TextView) view.findViewById(R.id.txtMajor);
        final TextView txtMinor = (TextView) view.findViewById(R.id.txtMinor);

        reader.getDataFromTag(tag, new NFCDataListener() {
            @Override
            public void NFCDataListener(String data) {
                txtMajor.setText(data);
                txtMinor.setVisibility(View.GONE);
            }

            @Override
            public void ParsedIDsFromNFCListener(int id2, int id3) {
                txtMajor.setText("Major: " + id2);
                major = id2;
                txtMinor.setText("Minor: " + id3);
                minor = id3;
            }

            @Override
            public void NFCnotSupported() {
                NFCNotWorkingDialog dialog = new NFCNotWorkingDialog();
                dialog.show(getFragmentManager().beginTransaction(), "dialog_nfc_not_working");
            }
        });
    }

    private void manageButtonBar(View view){
        Button btnPair = (Button) view.findViewById(R.id.btnPair);
        btnPair.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pair();
            }
        });
        Button btnCancel = (Button) view.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });
    }

    private void pair(){
        RangeController rangeController = new RangeController(context);

        String name = editName.getText().toString();
        if (name.equals("")){
            Toast.makeText(context, getString(R.string.nfc_no_name), Toast.LENGTH_LONG).show();
        } else {
            if (rangeController.checkIfInterested(major, minor) == null){
                rangeController.setInterestInDevice(name,major, minor);
                getActivity().finish();
            } else {
                Toast.makeText(context, getString(R.string.nfc_already_registered), Toast.LENGTH_LONG).show();
            }
        }
    }
}
