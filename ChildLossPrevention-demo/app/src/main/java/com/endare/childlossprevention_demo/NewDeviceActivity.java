package com.endare.childlossprevention_demo;

import android.app.Activity;
import android.app.Fragment;
import android.nfc.NfcAdapter;
import android.nfc.NfcManager;
import android.nfc.Tag;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.endare.bles.NFCReader;

public class NewDeviceActivity extends AppCompatActivity implements NfcAdapter.ReaderCallback {
    public static Tag discoveredTag;
    private NfcAdapter adapter;
    public static boolean nfcAvailable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_device);
        setTitle(getString(R.string.title_new_device));

        NfcManager manager = (NfcManager) getSystemService(NFC_SERVICE);
        adapter = manager.getDefaultAdapter();
        if (adapter != null){ //nfc is supported

            if (adapter.isEnabled()){
                setFragment(new NFCFragment());
                nfcAvailable = true;
            } else {
                //todo catch when NFC is possible but isn't enabled
                setFragment(new NFCFragment());
                nfcAvailable = true;
            }
        } else {
            nfcAvailable = false;

            setFragment(new NearbyFragment());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (nfcAvailable){
            adapter.enableReaderMode(this, this, NfcAdapter.FLAG_READER_NFC_A, null);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (nfcAvailable) {
            adapter.disableReaderMode(this);
        }
    }

    private void setFragment(Fragment fragment){
        getFragmentManager().beginTransaction().replace(R.id.newDeviceContainer, fragment).commit();
    }

    @Override
    public void onTagDiscovered(Tag tag) {
        discoveredTag = tag;
        new FragmentAnimator().slideRightFragment(getFragmentManager().beginTransaction(), new NFCRegistrationFragment(), R.id.newDeviceContainer);
    }
}
