package com.endare.childlossprevention_demo;

/**
 * Created by Francisco on 06/04/16.
 */
public interface TagDiscoveredListener {
    void onTagDiscovered();
}
