package com.endare.childlossprevention_demo;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.endare.bles.RangeController;
import com.estimote.sdk.Beacon;

/**
 * Created by Francisco on 06/04/16.
 */
public class AddNearbyDialog extends DialogFragment {
    private Beacon beacon;
    private EditText editName, editCode;
    private RangeController rangeController;
    private int pedotracker = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View dialogview = inflater.inflate(R.layout.dialog_add_nearby, container, false);
        beacon = NearbyFragment.selectedBeacon;
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        editName = (EditText) dialogview.findViewById(R.id.editName);
        editCode = (EditText) dialogview.findViewById(R.id.editCode);

        rangeController = new RangeController(dialogview.getContext());
        String code = rangeController.calculatePairingCode(beacon.getMajor(), beacon.getMinor());
        editCode.setText(code);

        Button btnPair = (Button) dialogview.findViewById(R.id.btnPair);
        btnPair.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editName.getText().equals("")){
                    Toast.makeText(dialogview.getContext(), getString(R.string.nfc_no_name), Toast.LENGTH_LONG).show();
                } else if (editName.getText().toString().equals("easter egg")){
                    Toast.makeText(dialogview.getContext(), "go search somewhere else for an easter egg", Toast.LENGTH_LONG).show();
                } else {
                    if (rangeController.checkIfInterested(beacon.getMajor(), beacon.getMinor()) == null){
                        if (rangeController.validatePairingCode(editCode.getText().toString(), beacon.getMajor(), beacon.getMinor())){
                            rangeController.setInterestInDevice(editName.getText().toString(), beacon.getMajor(), beacon.getMinor());
                            getActivity().finish();
                        } else {
                            pedotracker++;
                            if (pedotracker == 3){
                                Toast.makeText(dialogview.getContext(), "Don't be a pedo, go away", Toast.LENGTH_LONG).show();
                                pedotracker = 0;
                            } else {
                                Toast.makeText(dialogview.getContext(), getString(R.string.dialog_code_invalid), Toast.LENGTH_LONG).show();
                            }
                        }
                    } else {
                        Toast.makeText(dialogview.getContext(), getString(R.string.nfc_already_registered), Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        return dialogview;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            //give the dialog some width
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        }
    }
}
