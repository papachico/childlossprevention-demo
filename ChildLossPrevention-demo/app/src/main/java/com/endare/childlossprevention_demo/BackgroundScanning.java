package com.endare.childlossprevention_demo;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.TaskStackBuilder;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.endare.bles.DeviceDetectionListener;
import com.endare.bles.Scanner;
import com.estimote.sdk.Beacon;

import java.util.ArrayList;

/**
 * Created by Francisco on 12/02/16.
 *
 * This service handles the background scanning for beacons. This service is started at
 * boot, when bluetooth becomes enabled and when the MainActivity starts.
 */
public class BackgroundScanning extends Service {
    LocalBroadcastManager broadcastManager;
    public static ArrayList<Beacon> devicesInRange;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        broadcastManager = LocalBroadcastManager.getInstance(getApplicationContext());
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (!mBluetoothAdapter.isEnabled()) {
            mBluetoothAdapter.enable();
        }
        devicesInRange = new ArrayList<>();
        scan();
        return super.onStartCommand(intent, flags, startId);
    }

    public void scan(){
        Scanner scanner = new Scanner(this);
        scanner.startScanning(new DeviceDetectionListener() {
            @Override
            public void deviceEnterListener(Beacon beacon) {
                //Toast.makeText(getApplicationContext(), "A child is near ( ͡° \u035Cʖ ͡°)", Toast.LENGTH_SHORT).show();
                if (!devicesInRange.contains(beacon)){
                    devicesInRange.add(beacon);
                }
                update();
            }

            @Override
            public void interestedDeviceEnterListener(Beacon beacon, String name) {
                //Toast.makeText(getApplicationContext(), "Your child is in the area!", Toast.LENGTH_SHORT).show();
                if (!devicesInRange.contains(beacon)){
                    devicesInRange.add(beacon);
                }
                update();
                showNotification("Oef!",name + " is nearby, no need for mass hysteria. Yet...", getApplicationContext());
            }

            @Override
            public void deviceLeftListener(Beacon beacon) {
                //Toast.makeText(getApplicationContext(), "A child has left (>╭╮>))", Toast.LENGTH_SHORT).show();
                devicesInRange.remove(beacon);
                update();
            }

            @Override
            public void interestedDeviceLeftListener(Beacon beacon, String name) {
                //Toast.makeText(getApplicationContext(), "Your child is gone!", Toast.LENGTH_SHORT).show();
                devicesInRange.remove(beacon);
                update();
                showNotification("Quick!",name + " is gone!", getApplicationContext());
            }
        });
    }

    //send a broadcast to the activity that should be updated
    public void update(){
        Log.e("devices",devicesInRange.toString());
        Intent broadcastIntent = new Intent("com.endare.updateConnectedDevices");
        broadcastManager.sendBroadcast(broadcastIntent);
    }

    public void showNotification(String title, String message, Context context) {
        SharedPreferences sharedPreferences = getSharedPreferences("preferences", MODE_PRIVATE);
        if (sharedPreferences.getBoolean("notifications", true)){
            long[] pattern = {0, 200, 200, 200};

            Intent notifyIntent = new Intent(context, MainActivity.class);
            TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
            stackBuilder.addParentStack(MainActivity.class);
            stackBuilder.addNextIntent(notifyIntent);


            NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
            PendingIntent resultIntent = stackBuilder.getPendingIntent(1, PendingIntent.FLAG_UPDATE_CURRENT);


            builder.setAutoCancel(true)
                    .setContentTitle(title)
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                    .setContentText(message)
                    .setColor(context.getResources().getColor(R.color.colorPrimary))
                    .setSmallIcon(android.R.drawable.ic_menu_info_details)
                    .setPriority(Notification.PRIORITY_HIGH)
                    .setVibrate(pattern)
                    .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                    .setVisibility(Notification.VISIBILITY_PUBLIC)
                    .setContentIntent(resultIntent);


            NotificationManager notificationManager = (NotificationManager) context.getSystemService(context.NOTIFICATION_SERVICE);

            notificationManager.notify(1711, builder.build());
        }
    }
}
