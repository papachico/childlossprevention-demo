package com.endare.childlossprevention_demo;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by Francisco on 07/04/16.
 */
public class EditInterestDialog extends DialogFragment {
    private EditText editText;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View dialogview = inflater.inflate(R.layout.dialog_edit_interest, container, false);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        String name = getArguments().getString("name");

        editText = (EditText) dialogview.findViewById(R.id.editName);
        editText.setText(name);
        editText.setSelection(editText.getText().length());

        Button btnDelete = (Button) dialogview.findViewById(R.id.btnDelete);
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).deleteItem();
                getDialog().dismiss();
            }
        });

        Button btnSave = (Button) dialogview.findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).editName(editText.getText().toString());
                getDialog().dismiss();
            }
        });


        return dialogview;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            //give the dialog some width
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        }
    }
}
