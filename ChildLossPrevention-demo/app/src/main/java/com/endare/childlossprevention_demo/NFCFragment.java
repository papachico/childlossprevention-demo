package com.endare.childlossprevention_demo;

import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

public class NFCFragment extends Fragment {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.fragment_nfc, container, false);
        ImageView img = (ImageView) rootview.findViewById(R.id.imageNFC);
        showAnimation(img, 1000);

        Button btnNearby = (Button) rootview.findViewById(R.id.btnNearby);
        btnNearby.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new FragmentAnimator().slideLeftFragment(getFragmentManager().beginTransaction(), new NearbyFragment(), R.id.newDeviceContainer);
            }
        });

        return rootview;
    }


    private void showAnimation(final ImageView view, final int speed){
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            int[] imageArray = {R.drawable.nfc_animation_01,R.drawable.nfc_animation_02,R.drawable.nfc_animation_03};
            int i = 0;
            @Override
            public void run() {
                view.setImageResource(imageArray[i]);
                i++;
                if (i > imageArray.length - 1){
                    i = 0;
                }
                handler.postDelayed(this, speed);
            }
        };
        handler.postDelayed(runnable, 0);
    }
}
