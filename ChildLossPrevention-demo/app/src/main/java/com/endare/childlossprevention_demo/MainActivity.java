package com.endare.childlossprevention_demo;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.endare.bles.BLEDevice;
import com.endare.bles.RangeController;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private ListView list;
    private RangeController rangeController;
    private ArrayList<BLEDevice> listContent;
    private ItemAdapter adapter;
    private BroadcastReceiver receiver;
    private LocalBroadcastManager manager;
    private IntentFilter filter;
    private TextView noInterests;
    private int selectedInterest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        requestPermissions();

        noInterests = (TextView) findViewById(R.id.txtNoInterests);

        rangeController = new RangeController(this);
        list = (ListView) findViewById(R.id.listView);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectedInterest = position;
                EditInterestDialog dialog = new EditInterestDialog();
                Bundle bundle = new Bundle();
                bundle.putString("name", listContent.get(position).getName());
                dialog.setArguments(bundle);
                dialog.show(getFragmentManager(), "dialog_edit_interest");
            }
        });

        filter = new IntentFilter();
        filter.addAction("com.endare.updateConnectedDevices");
        manager = LocalBroadcastManager.getInstance(this);
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                setAllChildsSafe(true);
                listContent = rangeController.retrieveInterestedDevices();
                adapter.notifyDataSetChanged();
            }
        };


        handleFab();
        setAllChildsSafe(true); //todo update automatically

    }

    @Override
    protected void onResume() {
        super.onResume();
        listContent = rangeController.retrieveInterestedDevices();
        adapter = new ItemAdapter(this, R.layout.item_interested_device);
        list.setAdapter(adapter);
        manager.registerReceiver(receiver, filter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        manager.unregisterReceiver(receiver);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.menu_settings){
            startActivity(new Intent(this, SettingsActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    this.startService(new Intent(this, BackgroundScanning.class));
                } else {
                    finish();
                }
                return;
            }
        }
    }

    private void requestPermissions(){
        // check the API level of the device and handle the permission request when the
        // the device runs Marshmallow or a newer version of android
        if (Build.VERSION.SDK_INT > 22){
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED){
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
            } else {
                //start the backgroundscanning for beacons
                this.startService(new Intent(this, BackgroundScanning.class));
            }
        } else {
            //start the backgroundscanning for beacons
            this.startService(new Intent(this, BackgroundScanning.class));
        }
    }

    private void setAllChildsSafe(boolean safe){
        ImageButton imgbtnStatus = (ImageButton) findViewById(R.id.imgbtnStatus);
        if (safe){
            //imgbtnStatus.setImageDrawable(getResources().getDrawable(R.drawable.safe, null));
            imgbtnStatus.setImageResource(R.drawable.safe);
        } else {
            //imgbtnStatus.setImageDrawable(getResources().getDrawable(R.drawable.warning, null));
            imgbtnStatus.setImageResource(R.drawable.warning);
        }
    }

    private void handleFab(){
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fabAddChild);
        if (fab != null) {
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent newDeviceActivity = new Intent(getApplicationContext(), NewDeviceActivity.class);
                    startActivity(newDeviceActivity);
                }
            });
        }
    }

    public void editName(String name){
        BLEDevice selected = listContent.get(selectedInterest);
        rangeController.changeNameInterestedDevice(selected.getId2(), selected.getId3(), name);
        listContent.clear();
        listContent = rangeController.retrieveInterestedDevices();
        adapter.notifyDataSetChanged();
    }

    public void deleteItem(){
        rangeController.removeInterestInDeviceForIndex(selectedInterest);
        Toast.makeText(getApplicationContext(), getString(R.string.main_deleted), Toast.LENGTH_SHORT).show();
        listContent.remove(selectedInterest);
        if (listContent.size() == 0){
            noInterests.setVisibility(View.VISIBLE);
        }
        adapter.notifyDataSetChanged();
    }


    public class ItemAdapter extends ArrayAdapter {

        public ItemAdapter(Context context, int resource) {
            super(context, resource);
        }
        @Override
        public int getCount() {
            return listContent.size();
        }

        @Override
        public Object getItem(int position) {
            return listContent.get(position);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = getLayoutInflater().inflate(R.layout.item_interested_device, null);

            noInterests.setVisibility(View.GONE);

            TextView txtName = (TextView) convertView.findViewById(R.id.txtName);
            TextView txtMajor = (TextView) convertView.findViewById(R.id.txtMajor);
            TextView txtMinor = (TextView) convertView.findViewById(R.id.txtMinor);
            ImageView img = (ImageView) convertView.findViewById(R.id.imgStatusList);

            txtName.setText(listContent.get(position).getName());
            txtMajor.setText(listContent.get(position).getId2() + "");
            txtMinor.setText(listContent.get(position).getId3() + "");

            if (listContent.get(position).isInRange()){
                img.setImageResource(R.drawable.safe);
            } else {
                img.setImageResource(R.drawable.warning);
                setAllChildsSafe(false);
            }

            return convertView;
        }
    }
}
