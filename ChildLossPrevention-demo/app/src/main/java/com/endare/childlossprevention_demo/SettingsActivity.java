package com.endare.childlossprevention_demo;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Switch;
import android.widget.Toast;

import com.endare.bles.RangeController;

public class SettingsActivity extends AppCompatActivity {
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        setTitle(getString(R.string.title_settings));
        sharedPreferences = getSharedPreferences("preferences", MODE_PRIVATE);

        final Switch switchNotifications = (Switch) findViewById(R.id.switchNotifications);
        if (switchNotifications != null) {
            switchNotifications.setChecked(sharedPreferences.getBoolean("notifications", true));
            switchNotifications.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putBoolean("notifications", switchNotifications.isChecked());
                    editor.commit();
                }
            });
        }

        Button btnReset = (Button) findViewById(R.id.btnReset);
        if (btnReset != null) {
            btnReset.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    RangeController rangeController = new RangeController(getApplicationContext());
                    rangeController.removeAllInterests();
                    Toast.makeText(getApplicationContext(), getString(R.string.settings_reset_toast), Toast.LENGTH_LONG).show();
                }
            });
        }
    }
}
